import json
import requests
import pyppeteer
import asyncio

loop = asyncio.get_event_loop()

async def convert_page(url: str):
    browser = await pyppeteer.launch(
        {
            'pipe':
            True,
            'executablePath':
            'bin/headless-chromium',
            'args': [
                '--no-sandbox', '--disable-setuid-sandbox', '--disable-gpu',
                '--user-data-dir=/tmp', '--single-process',
                '--disable-dev-shm-usage'
            ]
        },
        handleSIGINT=False,
        handleSIGTERM=False,
        handleSIGHUP=False)
    page = await browser.newPage()
    await page.goto(url)
    await page.evaluateHandle(
        'document.fonts.ready'
    )  # with fast (ie local) pages, fonts don't load in time.
    await page.pdf({'path': '/tmp/print.out', 'format': 'A4'})
    return '/tmp/print.out'

def sync_chrome_convert(url: str) -> str:
    result = loop.run_until_complete(convert_page(url))
    return result

def hello(event, context):
    url = 'http://google.com'
    result = sync_chrome_convert(url)
    body = requests.get("https://jsonplaceholder.typicode.com/todos/").json()
    return {
        "statusCode": 200,
        "body": json.dumps(body)
    }
