APP_PORT := 5000

# Print this help message
help:
	@echo
	@awk '/^#/ {c=substr($$0,3); next} c && /^([a-zA-Z].+):/{ print "  \033[32m" $$1 "\033[0m",c }{c=0}' $(MAKEFILE_LIST) |\
	sort |\
	column -s: -t |\
	less -R


# Runs a local version of the serverless app
run/local:
	serverless offline

# Deploy to dev
deploy/dev:
	serverless deploy 

# Runs python tests
format:
	poetry run yapf -i *.py

# Runs python tests
test:
	poetry run pytest
